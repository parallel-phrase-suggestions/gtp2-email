## GPT-2 for emails

This model was developed for and used by the protype described in the paper [“The Impact of Multiple Parallel Phrase Suggestions on Email Input and Composition Behaviour of Native and Non-Native English Writers”](https://arxiv.org/abs/2101.09157). We built a text editor prototype with a neural language model (GPT-2) that allows to compose emails with different numbers of parallel suggestions.



###  Approach

We used the pre-trained [GPT-2 via HuggingFace](https://huggingface.co/gpt2) and finetuned it for our email use case on the well-known ENRON dataset (we used the lightly preprocessed version by [Brian Ray](https://data.world/brianray/enron-email-dataset)). We removed very short messages (e.g. informal one-liners), automated system messages, quoted replies and signatures and replaced dates and names with placeholders (which the backend filled in based on the study scenario). For finetuning, we used HuggingFace’s GPT-2 training script.



### Code

Prerequisites: Have `conda` installed (tested with `conda 4.8.2` and `Python 3.7.3` )



**Data preprocessing**

All preprocessing steps are done in the `enron.ipynb` notebook (run `anaconda-project run enron`).

We performed the following steps to generate the data used for fine-tuning: 

1. Remove emails that are irrelevant for our use case using custom rules (e.g., ones that start with `--` etc.)
2. We use EFZP to parse the email and only keep the body of the message
3. We stop the text of the email after defined keywords (e.g., `—`), timestamps or full email addresses. 
4. We replace repeated punctuation characters or control characters
5. We use spaCy to replace named entities (names, organizations, geopolitical entities, and relative dates) with special tags
6. We remove the special tags for names if they occur at the very beginning or very end of the text
7. We filter the emails so they have a certain text length and pass a set of manual rules.

Finally we store each email as a line in a text file, including the subject, a seperator token, and the body. 



**Fine-tuning**

The fine-tuning steps are done in the `sagemaker.ipynb` notebook (run `anaconda-project run sagemaker`).

For the training and inference we used a slightly modified version of HuggingFace’s boilerplate code and performed the training using SageMaker (an AWS service).

Training can be done either locally or remote using AWS-manages instances. We used the local training to verify and validate the setup and used the remote training to generate the actual model leveraging GPU instances. 

After the training we deploy the model to an endpoint (managed AWS instances serving the model), which can then be used to get model inferences. 

The resulting model can be found in the `fitting/output` folder.